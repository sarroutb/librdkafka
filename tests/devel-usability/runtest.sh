#!/bin/bash
set -e
set -x

TEST_SOURCE=kafka-test

C_TEST_TARGET="${TEST_SOURCE}.c"
CXX_TEST_TARGET="${TEST_SOURCE}.cpp"

C_BINARY="${TEST_SOURCE}-c"
CXX_BINARY="${TEST_SOURCE}-cpp"

CFLAGS="$(rpm --eval '%{build_cflags}')"
CXXFLAGS="$(rpm --eval '%{build_cxxflags}')"

LDFLAGS="$(rpm --eval '%{build_ldflags}')"

LIBCFLAGS="$(pkg-config rdkafka --libs --cflags)"
LIBCXXFLAGS="$(pkg-config rdkafka++ --libs --cflags)"

# build target using distribution-specific flags
gcc -std=c11 $CFLAGS $LDFLAGS $LIBCFLAGS -o $C_BINARY $C_TEST_TARGET
g++ -std=c++11 $CXXFLAGS $LDFLAGS $LIBCXXFLAGS -o $CXX_BINARY $CXX_TEST_TARGET


# test that target exists
test -f ./$C_BINARY
test -f ./$CXX_BINARY

# test that target is executable
test -x ./$C_BINARY
test -x ./$CXX_BINARY


# test that target runs successfully
./$C_BINARY -h
./$CXX_BINARY -h
